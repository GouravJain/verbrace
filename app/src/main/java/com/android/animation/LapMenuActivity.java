package com.android.animation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class LapMenuActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mLap1Tv, mLap2Tv, mLap3Tv, mLap4Tv, mLap5Tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lap_menu);
        mLap1Tv=(ImageView)findViewById(R.id.lap1_iv);
        mLap2Tv=(ImageView)findViewById(R.id.lap2_iv);
        mLap3Tv=(ImageView)findViewById(R.id.lap3_iv);
        mLap4Tv=(ImageView)findViewById(R.id.lap4_iv);
        mLap5Tv=(ImageView)findViewById(R.id.lap5_iv);

        mLap1Tv.setOnClickListener(this);
        mLap2Tv.setOnClickListener(this);
        mLap3Tv.setOnClickListener(this);
        mLap4Tv.setOnClickListener(this);
        mLap5Tv.setOnClickListener(this);
    }

    public static Intent getIntent(Context context) {
        Intent intent = new Intent(context, LapMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.lap1_iv:
                startLap(1);
                break;
            case R.id.lap2_iv:
                startLap(2);
                break;
            case R.id.lap3_iv:
                startLap(3);
                break;
            case R.id.lap4_iv:
                startLap(4);
                break;
            case R.id.lap5_iv:
                startLap(5);
                break;
        }
    }

    private void startLap(int lapChosen){
        Intent intent = new Intent(getApplicationContext(), RoadAnimationActivity.class);
        intent.putExtra("lapChosen",lapChosen);
        startActivity(intent);
    }
}
