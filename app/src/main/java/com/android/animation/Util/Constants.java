package com.android.animation.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Constants {
    //DATABASE CONSTANTS
    public static final String DATABASE_NAME = "db_verbrace";

    /* FIRST_LAP_TABLE table fields */
    public static final String TABLE_FIRST_LAP = "FirstLapTable";
    public static final String KEY_ID = "Id";
    public static final String KEY_HINDI_MEANING= "HindiMeaning";
    public static final String KEY_LAP_NO = "LapNo";
    public static final String KEY_ENGLISH_VERB1 = "V1-E";
    public static final String KEY_HINDI_VERB1 = "V1-H";
    public static final String KEY_ENGLISH_VERB2 = "V2-E";
    public static final String KEY_HINDI_VERB2 = "V2-H";
    public static final String KEY_ENGLISH_VERB3 = "V3-E";
    public static final String KEY_HINDI_VERB3 = "V3-H";
    public static final String KEY_ENGLISH_DVF = "DVF-E";
    public static final String KEY_HINDI_DVF = "DVF-H";

    /* FIRST_LAP_TABLE table fields */
    public static final String TABLE_SECOND_LAP = "SecondLapTable";
    /* FIRST_LAP_TABLE table fields */
    public static final String TABLE_THIRD_LAP = "ThirdLapTable";
    /* FIRST_LAP_TABLE table fields */
    public static final String TABLE_FOURTH_LAP = "FourthLapTable";
    /* FIRST_LAP_TABLE table fields */
    public static final String TABLE_FIFTH_LAP = "FifthLapTable";

    public static String changeDateFormat(String inputFormat, String outputFormat, String date) {
        String formattedDate = null;
        try {
            Date dt = new SimpleDateFormat(inputFormat).parse(date);
            formattedDate = new SimpleDateFormat(outputFormat).format(dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formattedDate;
    }
}