package com.android.animation.Util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import com.android.animation.model.VerbModel;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class SQLiteAdapter {
    private static final int DATABASE_VERSION = 1;

    public static SQLiteHelper sSqLiteHelper;
    private static String DB_PATH;//= Environment.getExternalStorageDirectory() + "/" + context.getPackageName() + "/";
    long result;
    private Runnable runnable;
    private SQLiteDatabase mSqLiteDatabase;
    private Context mContext;
    private Dialog mDialog;
    private SQLiteDbQueryListener sqLiteDbQueryListener;

    public SQLiteAdapter(Context c, SQLiteDbQueryListener listener, Dialog dialog) {
        mContext = c;
        sqLiteDbQueryListener = listener;
        mDialog = dialog;
        DB_PATH = Environment.getExternalStorageDirectory() + "/" + mContext.getPackageName() + "/";
        DB_PATH = "/data/data/" + mContext.getPackageName() + "/databases/";
        //call it so db get copied from assets to sdcard
        openToRead();
        close();
    }

    public void showProgressDialog() {
        Activity activity = ((Activity) mContext);
        if (activity.isFinishing()) {
            return;
        }

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (null != mDialog && !mDialog.isShowing())
                    mDialog.show();
            }
        });
    }

    public void hideProgressDialogue() {
        if (null != mDialog)
            mDialog.dismiss();
    }

    private SQLiteAdapter openToRead() throws android.database.SQLException {
        sSqLiteHelper = new SQLiteHelper(mContext, Constants.DATABASE_NAME, null, DATABASE_VERSION);
        mSqLiteDatabase = sSqLiteHelper.getReadableDatabase();
        return this;
    }

    private SQLiteAdapter openToWrite() throws android.database.SQLException {
        sSqLiteHelper = new SQLiteHelper(mContext, Constants.DATABASE_NAME, null, DATABASE_VERSION);
        mSqLiteDatabase = sSqLiteHelper.getWritableDatabase();
        return this;
    }

    private void close() {
        sSqLiteHelper.close();
    }

    /**
     * Copies your database from your local assets-folder to the just created empty database in the
     * system folder, from where it can be accessed and handled.
     * This is done by transferring byte-stream.
     */
    private void copyDataBase() throws IOException {
        //Open your local db as the input stream
        InputStream myInput = mContext.getAssets().open(Constants.DATABASE_NAME);
        // Path to the just created empty db
        String outFileName = DB_PATH + Constants.DATABASE_NAME;
        //Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);
        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        //Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    public List<VerbModel> getWordByLap(final int currentLap) {
        final List<VerbModel> lstVerbModels = new ArrayList<VerbModel>();
        //showProgressDialog();
        runnable = new Runnable() {
            @Override
            public void run() {
                openToRead();
                String Query="";
                switch (currentLap) {
                    case 1:
                        Query = "SELECT * FROM " + Constants.TABLE_FIRST_LAP;
                        break;
                    case 2:
                        Query = "SELECT * FROM " + Constants.TABLE_SECOND_LAP;
                        break;
                    case 3:
                        Query = "SELECT * FROM " + Constants.TABLE_THIRD_LAP;
                        break;
                    case 4:
                        Query = "SELECT * FROM " + Constants.TABLE_FOURTH_LAP;
                        break;
                    case 5:
                        Query = "SELECT * FROM " + Constants.TABLE_FIFTH_LAP;
                        break;
                }

                Cursor cursor = mSqLiteDatabase.rawQuery(Query, null);

                int i = 0;
                if (cursor.moveToFirst()) {
                    while (cursor.isAfterLast() == false) {
                        String categoryId, categoryName, categoryIcon;
                        String Id, HindiMeaning, englishVerb1, hindiVerb1, englishVerb2, hindiVerb2, englishVerb3, hindiVerb3, englishDVF, hindiDVF;
                        int index_ID = cursor.getColumnIndex(Constants.KEY_ID);
                        int index_HINDI_MEANING = cursor.getColumnIndex(Constants.KEY_HINDI_MEANING);
                        int index_ENGLISH_VERB1 = cursor.getColumnIndex(Constants.KEY_ENGLISH_VERB1);
                        int index_HINDI_VERB1 = cursor.getColumnIndex(Constants.KEY_HINDI_VERB1);
                        int index_ENGLISH_VERB2 = cursor.getColumnIndex(Constants.KEY_ENGLISH_VERB2);
                        int index_HINDI_VERB2 = cursor.getColumnIndex(Constants.KEY_HINDI_VERB3);
                        int index_ENGLISH_VERB3 = cursor.getColumnIndex(Constants.KEY_ENGLISH_VERB3);
                        int index_HINDI_VERB3 = cursor.getColumnIndex(Constants.KEY_HINDI_VERB3);
                        int index_ENGLISH_DVF = cursor.getColumnIndex(Constants.KEY_ENGLISH_DVF);
                        int index_HINDI_DVF = cursor.getColumnIndex(Constants.KEY_HINDI_DVF);

                        Id = cursor.getString(index_ID);
                        HindiMeaning = cursor.getString(index_HINDI_MEANING);
                        englishVerb1 = cursor.getString(index_ENGLISH_VERB1);
                        hindiVerb1 = cursor.getString(index_HINDI_VERB1);
                        englishVerb2 = cursor.getString(index_ENGLISH_VERB2);
                        hindiVerb2 = cursor.getString(index_HINDI_VERB2);
                        englishVerb3 = cursor.getString(index_ENGLISH_VERB3);
                        hindiVerb3 = cursor.getString(index_HINDI_VERB3);
                        englishDVF = cursor.getString(index_ENGLISH_DVF);
                        hindiDVF = cursor.getString(index_HINDI_DVF);
                        VerbModel verbModel = new VerbModel(Id, HindiMeaning, englishVerb1, hindiVerb1, englishVerb2, hindiVerb2, englishVerb3, hindiVerb3, englishDVF, hindiDVF, currentLap);
                        lstVerbModels.add(verbModel);
                        cursor.moveToNext();
                    }
                }
                close();
                if (null != sqLiteDbQueryListener) {
                    ((Activity) mContext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideProgressDialogue();
                            sqLiteDbQueryListener.onSuccess(currentLap);
                        }
                    });
                    return;
                }
            }
        };
        new Thread(runnable).start();
        return lstVerbModels;
    }

    public interface SQLiteDbQueryListener {
        void onSuccess(int reqCode);

        void onCancel(boolean canceled);
    }

    /**
     * Provides functionality for data definition
     */
    public class SQLiteHelper extends SQLiteOpenHelper {
        public SQLiteHelper(Context context, String name, CursorFactory factory, int version) {
            //			super(context, name, factory, version);
            super(context, DB_PATH + name, null, version);
        }

        /**
         * Creates a empty database on the system and rewrites it with your own database.
         */
        public void copyDBtoSDCard() throws IOException {
            this.getReadableDatabase();
            try {
                copyDataBase();
                Log.d("SqliteAdapter:", "DB Copied");
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }

        /**
         * Handle on database create
         */
        @Override
        public void onCreate(SQLiteDatabase db) {
        }

        /**
         * Handle on database update
         */
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }
}