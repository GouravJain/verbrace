package com.android.animation.Util;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * A utility class that contains different utility methods.
 * <p/>
 * Created by Gourav on 4/30/2015.
 */
public class Utility {

    private static final String TAG = Utility.class.getSimpleName();
    public static String changeDateFormat(String inputFormat, String outputFormat, String date) {
        String formattedDate = null;
        try {
            Date dt = new SimpleDateFormat(inputFormat).parse(date);
            formattedDate = new SimpleDateFormat(outputFormat).format(dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    /**
     * Generate random integers in a range
     *
     * @param min - minimum value of range
     * @param max - maximum value of range
     * @return
     */
    public static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    public static String getPath(Uri uri, Context context) {
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = ((Activity)context).managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        projection = null;
        return cursor.getString(column_index);
    }
}