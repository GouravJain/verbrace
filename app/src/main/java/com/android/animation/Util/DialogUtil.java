package com.android.animation.Util;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.android.animation.R;


public class DialogUtil {

    public static Dialog createProgressDialog(Context context) {
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    public static void showOkDialog(final Context context, String title, String message) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.setContentView(R.layout.ok_dialog);
        TextView titleTv = (TextView) dialog.findViewById(R.id.title_tv);
        TextView messageTv = (TextView) dialog.findViewById(R.id.message_tv);
        titleTv.setText(title);
        messageTv.setText(Html.fromHtml(message));

        TextView mOkTv = (TextView) dialog.findViewById(R.id.ok_tv);
        mOkTv.setTag(dialog);
        mOkTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static Dialog showOkDialog(final Context context, String title, String message, View.OnClickListener listener) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.setContentView(R.layout.ok_dialog);
        TextView titleTv = (TextView) dialog.findViewById(R.id.title_tv);
        TextView messageTv = (TextView) dialog.findViewById(R.id.message_tv);
        titleTv.setText(title);
        messageTv.setText(Html.fromHtml(message));

        TextView mOkTv = (TextView) dialog.findViewById(R.id.ok_tv);
        mOkTv.setTag(dialog);
        mOkTv.setOnClickListener(listener);

        dialog.show();
        return dialog;
    }

    public static Dialog showLapOverDialog(final Context context, String message, View.OnClickListener listener) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.setContentView(R.layout.lap_over_dialog);
        TextView messageTv = (TextView) dialog.findViewById(R.id.message_tv);
        messageTv.setText(Html.fromHtml(message));

//        TextView mOkTv = (TextView) dialog.findViewById(R.id.ok_tv);
//        mOkTv.setTag(dialog);
        dialog.findViewById(R.id.lap_over_dialog_ll).setOnClickListener(listener);

        dialog.show();
        return dialog;
    }

    public static Dialog showOkCancelDialog(final Context context, String title, String message, View.OnClickListener okListener) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.setContentView(R.layout.ok_cancel_dialog);
        TextView titleTv = (TextView) dialog.findViewById(R.id.title_tv);
        TextView messageTv = (TextView) dialog.findViewById(R.id.message_tv);
        titleTv.setText(title);
        messageTv.setText(Html.fromHtml(message));

        TextView mOkTv = (TextView) dialog.findViewById(R.id.ok_tv);
        TextView mCancelTv = (TextView) dialog.findViewById(R.id.cancel_tv);
        mOkTv.setTag(dialog);
        mOkTv.setOnClickListener(okListener);
        mCancelTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        return dialog;
    }

    public static Dialog showOkCancelDialog(final Context context, String title, String message, View.OnClickListener okListener, View.OnClickListener cancelListener) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.setContentView(R.layout.ok_cancel_dialog);
        TextView titleTv = (TextView) dialog.findViewById(R.id.title_tv);
        TextView messageTv = (TextView) dialog.findViewById(R.id.message_tv);
        titleTv.setText(title);
        messageTv.setText(Html.fromHtml(message));

        TextView mOkTv = (TextView) dialog.findViewById(R.id.ok_tv);
        TextView mCancelTv = (TextView) dialog.findViewById(R.id.cancel_tv);
        mOkTv.setTag(dialog);
        mOkTv.setOnClickListener(okListener);
        mCancelTv.setOnClickListener(cancelListener);

        dialog.show();
        return dialog;
    }

    public static void ShowToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }
}