package com.android.animation;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

public class BaseActivity extends Activity {
    private Dialog dialog;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_start, R.anim.activity_stop);
        finish();
    }

    public void startActivityWithAnim(Intent intent) {
        startActivity(intent);
        overridePendingTransition(R.anim.activity_start, R.anim.activity_stop);
    }

    public void finishActivityWithAnim() {
        finish();
        overridePendingTransition(R.anim.activity_start, R.anim.activity_stop);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
