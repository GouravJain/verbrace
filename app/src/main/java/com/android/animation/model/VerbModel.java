package com.android.animation.model;

/**
 * Created by Gourav on 10-04-2015.
 */
public class VerbModel {
    private String Id, HindiMeaning, englishVerb1, hindiVerb1, englishVerb2, hindiVerb2, englishVerb3, hindiVerb3, englishDVF, hindiDVF;
    private int LapNo;

    public VerbModel(String id, String hindiMeaning, String englishVerb1, String hindiVerb1, String englishVerb2, String hindiVerb2, String englishVerb3, String hindiVerb3, String englishDVF, String hindiDVF, int lapNo) {
        Id = id;
        HindiMeaning = hindiMeaning;
        this.englishVerb1 = englishVerb1;
        this.hindiVerb1 = hindiVerb1;
        this.englishVerb2 = englishVerb2;
        this.hindiVerb2 = hindiVerb2;
        this.englishVerb3 = englishVerb3;
        this.hindiVerb3 = hindiVerb3;
        this.englishDVF = englishDVF;
        this.hindiDVF = hindiDVF;
        LapNo = lapNo;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getHindiMeaning() {
        return HindiMeaning;
    }

    public void setHindiMeaning(String hindiMeaning) {
        HindiMeaning = hindiMeaning;
    }

    public String getEnglishVerb1() {
        return englishVerb1;
    }

    public void setEnglishVerb1(String englishVerb1) {
        this.englishVerb1 = englishVerb1;
    }

    public String getHindiVerb1() {
        return hindiVerb1;
    }

    public void setHindiVerb1(String hindiVerb1) {
        this.hindiVerb1 = hindiVerb1;
    }

    public String getEnglishVerb2() {
        return englishVerb2;
    }

    public void setEnglishVerb2(String englishVerb2) {
        this.englishVerb2 = englishVerb2;
    }

    public String getHindiVerb2() {
        return hindiVerb2;
    }

    public void setHindiVerb2(String hindiVerb2) {
        this.hindiVerb2 = hindiVerb2;
    }

    public String getEnglishVerb3() {
        return englishVerb3;
    }

    public void setEnglishVerb3(String englishVerb3) {
        this.englishVerb3 = englishVerb3;
    }

    public String getHindiVerb3() {
        return hindiVerb3;
    }

    public void setHindiVerb3(String hindiVerb3) {
        this.hindiVerb3 = hindiVerb3;
    }

    public String getEnglishDVF() {
        return englishDVF;
    }

    public void setEnglishDVF(String englishDVF) {
        this.englishDVF = englishDVF;
    }

    public String getHindiDVF() {
        return hindiDVF;
    }

    public void setHindiDVF(String hindiDVF) {
        this.hindiDVF = hindiDVF;
    }

    public int getLapNo() {
        return LapNo;
    }

    public void setLapNo(int lapNo) {
        LapNo = lapNo;
    }
}
