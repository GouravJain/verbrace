package com.android.animation;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.animation.Util.DialogUtil;
import com.android.animation.Util.SQLiteAdapter;
import com.android.animation.Util.Utility;
import com.android.animation.model.VerbModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class RoadAnimationActivity extends Activity implements View.OnClickListener, SQLiteAdapter.SQLiteDbQueryListener {
    private static List<VerbModel> lstVerbModel = new ArrayList<VerbModel>();
    final View.OnClickListener finishListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };
    ImageView lifeIndicatorIv, answerChb;
    View mLeftCarVw, mCenterCarVw, mRightCarVw, mLeftRoadVw, mRightRoadVw, mCenterRoadVw;
    LinearLayout mAnswer1Ll, mAnswer2Ll, mAnswer3Ll, mAnswer4Ll, mAnswer5Ll, mAnswer6Ll;
    TextView mAnswer1Tv, mAnswer2Tv, mAnswer3Tv, mAnswer4Tv, mAnswer5Tv, mAnswer6Tv;
    TextView mAnswer1HindiTv, mAnswer2HindiTv, mAnswer3HindiTv, mAnswer4HindiTv, mAnswer5HindiTv, mAnswer6HindiTv;
    TextView mAnswerTv, mAnswerV1Tv, mAnswerV2Tv, mAnswerV3Tv, mQuestionTv, mLifeRemainingTv;
    ImageView mPauseIv, mAccelerometerIv;//, mHeaderV1Tv, mHeaderV2Tv, mHeaderV3Tv;
    int timer1 = 11, timer2 = 11, timer3 = 11, timer4 = 11, timer5 = 11, timer6 = 11, mCurrentColumn = 0;
    Animation dashLineAnimation, answers1Animation, answers2Animation, answers3Animation, answers4Animation, answers5Animation, answers6Animation;
    private int currentLap = 1, currentQuestionNo = 1, flagToSetAnswer = 0, lifeRemaining = 10, continueWrongHit = 0, currSpeedInSecond=2200, inboundTimeLimit=6, outBoundTimeLimit=8, timeAllowedInSeconds=10;
    private int answerType = 1;//1=Verb1;2=Verb2;3=Verb3
    private SQLiteAdapter sqLiteAdapter;
    private boolean isPaused = false;
    private Dialog mDialog;
    final View.OnClickListener continueListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mDialog.dismiss();
            isPaused = false;
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_game);
        try {
            sqLiteAdapter = new SQLiteAdapter(this, this, DialogUtil.createProgressDialog(this));
            SQLiteAdapter.sSqLiteHelper.copyDBtoSDCard();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }
        currentLap = getIntent().getIntExtra("lapChosen", 1);
        lstVerbModel = sqLiteAdapter.getWordByLap(currentLap);
    }

    private void initialization() {
        mAnswer1Ll = (LinearLayout) findViewById(R.id.answer1_ll);
        mAnswer2Ll = (LinearLayout) findViewById(R.id.answer2_ll);
        mAnswer3Ll = (LinearLayout) findViewById(R.id.answer3_ll);
        mAnswer4Ll = (LinearLayout) findViewById(R.id.answer4_ll);
        mAnswer5Ll = (LinearLayout) findViewById(R.id.answer5_ll);
        mAnswer6Ll = (LinearLayout) findViewById(R.id.answer6_ll);
        mAnswer1Tv = (TextView) findViewById(R.id.answer1_tv);
        mAnswer2Tv = (TextView) findViewById(R.id.answer2_tv);
        mAnswer3Tv = (TextView) findViewById(R.id.answer3_tv);
        mAnswer4Tv = (TextView) findViewById(R.id.answer4_tv);
        mAnswer5Tv = (TextView) findViewById(R.id.answer5_tv);
        mAnswer6Tv = (TextView) findViewById(R.id.answer6_tv);
        mAnswer1HindiTv = (TextView) findViewById(R.id.answer1_hindi_tv);
        mAnswer2HindiTv = (TextView) findViewById(R.id.answer2_hindi_tv);
        mAnswer3HindiTv = (TextView) findViewById(R.id.answer3_hindi_tv);
        mAnswer4HindiTv = (TextView) findViewById(R.id.answer4_hindi_tv);
        mAnswer5HindiTv = (TextView) findViewById(R.id.answer5_hindi_tv);
        mAnswer6HindiTv = (TextView) findViewById(R.id.answer6_hindi_tv);
        mAnswerTv = (TextView) findViewById(R.id.answer_tv);
        mAnswerV1Tv = (TextView) findViewById(R.id.answer_v1_tv);
        mAnswerV2Tv = (TextView) findViewById(R.id.answer_v2_tv);
        mAnswerV3Tv = (TextView) findViewById(R.id.answer_v3_tv);
        mQuestionTv = (TextView) findViewById(R.id.question_tv);
        mLifeRemainingTv = (TextView) findViewById(R.id.life_count_tv);
        mPauseIv = (ImageView) findViewById(R.id.pause_iv);
        mAccelerometerIv= (ImageView) findViewById(R.id.accelerator_iv);
        //        mHeaderV1Tv = (TextView) findViewById(R.id.header_v1_tv);
        //        mHeaderV2Tv = (TextView) findViewById(R.id.header_v2_tv);
        //        mHeaderV3Tv = (TextView) findViewById(R.id.header_v3_tv);
        lifeIndicatorIv = (ImageView) findViewById(R.id.life_indicator_iv);
        answerChb = (ImageView) findViewById(R.id.checkbox);

        mLeftCarVw = findViewById(R.id.left_car_iv);
        mRightCarVw = findViewById(R.id.right_car_iv);
        mCenterCarVw = findViewById(R.id.center_car_ll);
        mLeftRoadVw = findViewById(R.id.left_road_vw);
        mCenterRoadVw = findViewById(R.id.center_road_vw);
        mRightRoadVw = findViewById(R.id.right_road_vw);

        mLeftRoadVw.setOnClickListener(this);
        mCenterRoadVw.setOnClickListener(this);
        mRightRoadVw.setOnClickListener(this);
        mPauseIv.setOnClickListener(this);
        mAccelerometerIv.setOnClickListener(this);

        answers1Animation = AnimationUtils.loadAnimation(this, R.anim.answers_animation);
        answers4Animation = AnimationUtils.loadAnimation(this, R.anim.answers_animation);
        answers2Animation = AnimationUtils.loadAnimation(this, R.anim.answers_animation);
        answers5Animation = AnimationUtils.loadAnimation(this, R.anim.answers_animation);
        answers3Animation = AnimationUtils.loadAnimation(this, R.anim.answers_animation);
        answers6Animation = AnimationUtils.loadAnimation(this, R.anim.answers_animation);

        answers1Animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mAnswer1Ll.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mAnswer1Ll.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        answers2Animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mAnswer2Ll.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mAnswer2Ll.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        answers3Animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mAnswer3Ll.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mAnswer3Ll.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        answers4Animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mAnswer4Ll.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mAnswer4Ll.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        answers5Animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mAnswer5Ll.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mAnswer5Ll.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        answers6Animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mAnswer6Ll.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mAnswer6Ll.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        dashLineAnimation = AnimationUtils.loadAnimation(this, R.anim.dashed_bar_animation);
    }

    private void setQuestion() {
        mQuestionTv.setText(lstVerbModel.get(currentQuestionNo - 1).getHindiMeaning() + "");
    }

    Timer speedometerTimer;
    private void launchAnswerRegularly() {
        //launch new answer in every 2.2 second
        speedometerTimer = new Timer();
        speedometerTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (!isPaused) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // task to be done every seconds
                            int column = getCurrentColumn();
                            if (column == 1 && (timer1 > 1 && timer1 < 6)) {
                                column = 2;
                            }
                            if (column == 2 && (timer2 > 1 && timer2 < 6)) {
                                column = 3;
                            }
                            if (column == 3 && (timer3 > 1 && timer3 < 6)) {
                                column = 1;
                            }

                            switch (column) {
                                case 1:
                                    if (timer1 < 1 || timer1 > timeAllowedInSeconds) {
                                        timer1 = 0;
                                        if (++flagToSetAnswer % 4 == 0) {
                                            switch (answerType) {
                                                case 1:
                                                    mAnswer1Tv.setText(lstVerbModel.get(currentQuestionNo - 1).getEnglishVerb1());
                                                    mAnswer1HindiTv.setText(lstVerbModel.get(currentQuestionNo - 1).getHindiVerb1());
                                                    break;
                                                case 2:
                                                    mAnswer1Tv.setText(lstVerbModel.get(currentQuestionNo - 1).getEnglishVerb2());
                                                    mAnswer1HindiTv.setText(lstVerbModel.get(currentQuestionNo - 1).getHindiVerb2());
                                                    break;
                                                case 3:
                                                    mAnswer1Tv.setText(lstVerbModel.get(currentQuestionNo - 1).getEnglishVerb3());
                                                    mAnswer1HindiTv.setText(lstVerbModel.get(currentQuestionNo - 1).getHindiVerb3());
                                                    break;
                                            }

                                        } else {
                                            int random = Utility.randInt(1, 20);
                                            if (answerType == 1) {
                                                mAnswer1Tv.setText(lstVerbModel.get(random).getEnglishVerb1());
                                                mAnswer1HindiTv.setText(lstVerbModel.get(random).getHindiVerb1());
                                            } else {
                                                mAnswer1Tv.setText(lstVerbModel.get(random).getEnglishDVF());
                                                mAnswer1HindiTv.setText(lstVerbModel.get(random).getHindiDVF());
                                            }
                                        }
                                        mAnswer1Ll.startAnimation(answers1Animation);
                                    } else {
                                        timer4 = 0;
                                        if (++flagToSetAnswer % 4 == 0) {
                                            switch (answerType) {
                                                case 1:
                                                    mAnswer4Tv.setText(lstVerbModel.get(currentQuestionNo - 1).getEnglishVerb1());
                                                    mAnswer4HindiTv.setText(lstVerbModel.get(currentQuestionNo - 1).getHindiVerb1());
                                                    break;
                                                case 2:
                                                    mAnswer4Tv.setText(lstVerbModel.get(currentQuestionNo - 1).getEnglishVerb2());
                                                    mAnswer4HindiTv.setText(lstVerbModel.get(currentQuestionNo - 1).getHindiVerb2());
                                                    break;
                                                case 3:
                                                    mAnswer4Tv.setText(lstVerbModel.get(currentQuestionNo - 1).getEnglishVerb3());
                                                    mAnswer4HindiTv.setText(lstVerbModel.get(currentQuestionNo - 1).getHindiVerb3());
                                                    break;
                                            }
                                        } else {
                                            int random = Utility.randInt(1, 20);
                                            if (answerType == 1) {
                                                mAnswer4Tv.setText(lstVerbModel.get(random).getEnglishVerb1());
                                                mAnswer4HindiTv.setText(lstVerbModel.get(random).getHindiVerb1());
                                            } else {
                                                mAnswer4Tv.setText(lstVerbModel.get(random).getEnglishDVF());
                                                mAnswer4HindiTv.setText(lstVerbModel.get(random).getHindiDVF());
                                            }
                                        }
                                        mAnswer4Ll.startAnimation(answers4Animation);
                                    }
                                    break;
                                case 2:
                                    if (timer2 < 1 || timer2 > timeAllowedInSeconds) {
                                        timer2 = 0;
                                        if (++flagToSetAnswer % 4 == 0) {
                                            switch (answerType) {
                                                case 1:
                                                    mAnswer2Tv.setText(lstVerbModel.get(currentQuestionNo - 1).getEnglishVerb1());
                                                    mAnswer2HindiTv.setText(lstVerbModel.get(currentQuestionNo - 1).getHindiVerb1());
                                                    break;
                                                case 2:
                                                    mAnswer2Tv.setText(lstVerbModel.get(currentQuestionNo - 1).getEnglishVerb2());
                                                    mAnswer2HindiTv.setText(lstVerbModel.get(currentQuestionNo - 1).getHindiVerb2());
                                                    break;
                                                case 3:
                                                    mAnswer2Tv.setText(lstVerbModel.get(currentQuestionNo - 1).getEnglishVerb3());
                                                    mAnswer2HindiTv.setText(lstVerbModel.get(currentQuestionNo - 1).getHindiVerb3());
                                                    break;
                                            }
                                        } else {
                                            int random = Utility.randInt(1, 20);
                                            if (answerType == 1) {
                                                mAnswer2Tv.setText(lstVerbModel.get(random).getEnglishVerb1());
                                                mAnswer2HindiTv.setText(lstVerbModel.get(random).getHindiVerb1());
                                            } else {
                                                mAnswer2Tv.setText(lstVerbModel.get(random).getEnglishDVF());
                                                mAnswer2HindiTv.setText(lstVerbModel.get(random).getHindiDVF());
                                            }
                                        }
                                        mAnswer2Ll.startAnimation(answers2Animation);
                                    } else {
                                        timer5 = 0;
                                        if (++flagToSetAnswer % 4 == 0) {
                                            switch (answerType) {
                                                case 1:
                                                    mAnswer5Tv.setText(lstVerbModel.get(currentQuestionNo - 1).getEnglishVerb1());
                                                    mAnswer5HindiTv.setText(lstVerbModel.get(currentQuestionNo - 1).getHindiVerb1());
                                                    break;
                                                case 2:
                                                    mAnswer5Tv.setText(lstVerbModel.get(currentQuestionNo - 1).getEnglishVerb2());
                                                    mAnswer5HindiTv.setText(lstVerbModel.get(currentQuestionNo - 1).getHindiVerb2());
                                                    break;
                                                case 3:
                                                    mAnswer5Tv.setText(lstVerbModel.get(currentQuestionNo - 1).getEnglishVerb3());
                                                    mAnswer5HindiTv.setText(lstVerbModel.get(currentQuestionNo - 1).getHindiVerb3());
                                                    break;
                                            }
                                        } else {
                                            int random = Utility.randInt(1, 20);
                                            if (answerType == 1) {
                                                mAnswer5Tv.setText(lstVerbModel.get(random).getEnglishVerb1());
                                                mAnswer5HindiTv.setText(lstVerbModel.get(random).getHindiVerb1());
                                            } else {
                                                mAnswer5Tv.setText(lstVerbModel.get(random).getEnglishDVF());
                                                mAnswer5HindiTv.setText(lstVerbModel.get(random).getHindiDVF());
                                            }
                                        }
                                        mAnswer5Ll.startAnimation(answers5Animation);
                                    }
                                    break;
                                case 3:
                                    if (timer3 < 1 || timer3 > timeAllowedInSeconds) {
                                        timer3 = 0;
                                        if (++flagToSetAnswer % 4 == 0) {
                                            switch (answerType) {
                                                case 1:
                                                    mAnswer3Tv.setText(lstVerbModel.get(currentQuestionNo - 1).getEnglishVerb1());
                                                    mAnswer3HindiTv.setText(lstVerbModel.get(currentQuestionNo - 1).getHindiVerb1());
                                                    break;
                                                case 2:
                                                    mAnswer3Tv.setText(lstVerbModel.get(currentQuestionNo - 1).getEnglishVerb2());
                                                    mAnswer3HindiTv.setText(lstVerbModel.get(currentQuestionNo - 1).getHindiVerb2());
                                                    break;
                                                case 3:
                                                    mAnswer3Tv.setText(lstVerbModel.get(currentQuestionNo - 1).getEnglishVerb3());
                                                    mAnswer3HindiTv.setText(lstVerbModel.get(currentQuestionNo - 1).getHindiVerb3());
                                                    break;
                                            }
                                        } else {
                                            int random = Utility.randInt(1, 20);
                                            if (answerType == 1) {
                                                mAnswer3Tv.setText(lstVerbModel.get(random).getEnglishVerb1());
                                                mAnswer3HindiTv.setText(lstVerbModel.get(random).getHindiVerb1());
                                            } else {
                                                mAnswer3Tv.setText(lstVerbModel.get(random).getEnglishDVF());
                                                mAnswer3HindiTv.setText(lstVerbModel.get(random).getHindiDVF());
                                            }
                                        }
                                        mAnswer3Ll.startAnimation(answers3Animation);
                                    } else {
                                        timer6 = 0;
                                        if (++flagToSetAnswer % 4 == 0) {
                                            switch (answerType) {
                                                case 1:
                                                    mAnswer6Tv.setText(lstVerbModel.get(currentQuestionNo - 1).getEnglishVerb1());
                                                    mAnswer6HindiTv.setText(lstVerbModel.get(currentQuestionNo - 1).getHindiVerb1());
                                                    break;
                                                case 2:
                                                    mAnswer6Tv.setText(lstVerbModel.get(currentQuestionNo - 1).getEnglishVerb2());
                                                    mAnswer6HindiTv.setText(lstVerbModel.get(currentQuestionNo - 1).getHindiVerb2());
                                                    break;
                                                case 3:
                                                    mAnswer6Tv.setText(lstVerbModel.get(currentQuestionNo - 1).getEnglishVerb3());
                                                    mAnswer6HindiTv.setText(lstVerbModel.get(currentQuestionNo - 1).getHindiVerb3());
                                                    break;
                                            }
                                        } else {
                                            int random = Utility.randInt(1, 20);
                                            if (answerType == 1) {
                                                mAnswer6Tv.setText(lstVerbModel.get(random).getEnglishVerb1());
                                                mAnswer6HindiTv.setText(lstVerbModel.get(random).getHindiVerb1());
                                            } else {
                                                mAnswer6Tv.setText(lstVerbModel.get(random).getEnglishDVF());
                                                mAnswer6HindiTv.setText(lstVerbModel.get(random).getHindiDVF());
                                            }
                                        }
                                        mAnswer6Ll.startAnimation(answers6Animation);
                                    }
                                    break;
                            }
                        }
                    });
                }
            }
        }, 0, currSpeedInSecond);
    }

    private void validateAnswer() {
        Timer oneSecondTimer = new Timer();
        oneSecondTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (!isPaused) {
                    // task to be done every seconds
                    timer1++;
                    timer2++;
                    timer3++;
                    timer4++;
                    timer5++;
                    timer6++;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if ((timer1 > inboundTimeLimit && timer1 < outBoundTimeLimit)) {
                                if (mLeftCarVw.getVisibility() == View.VISIBLE) {
                                    String answerHinted = mAnswer1Tv.getText().toString();
                                    checkAnswer(answerHinted);
                                }
                            }
                            if ((timer4 > inboundTimeLimit && timer4 < outBoundTimeLimit)) {
                                if (mLeftCarVw.getVisibility() == View.VISIBLE) {
                                    String answerHinted = mAnswer4Tv.getText().toString();
                                    checkAnswer(answerHinted);
                                }
                            }
                            if ((timer2 > inboundTimeLimit && timer2 < outBoundTimeLimit)) {
                                if (mCenterCarVw.getVisibility() == View.VISIBLE) {
                                    String answerHinted = mAnswer2Tv.getText().toString();
                                    checkAnswer(answerHinted);
                                }
                            }
                            if ((timer5 > inboundTimeLimit && timer5 < outBoundTimeLimit)) {
                                if (mCenterCarVw.getVisibility() == View.VISIBLE) {
                                    String answerHinted = mAnswer5Tv.getText().toString();
                                    checkAnswer(answerHinted);
                                }
                            }
                            if ((timer3 > inboundTimeLimit && timer3 < outBoundTimeLimit)) {
                                if (mRightCarVw.getVisibility() == View.VISIBLE) {
                                    String answerHinted = mAnswer3Tv.getText().toString();
                                    checkAnswer(answerHinted);
                                }
                            }
                            if ((timer6 > inboundTimeLimit && timer6 < outBoundTimeLimit)) {
                                if (mRightCarVw.getVisibility() == View.VISIBLE) {
                                    String answerHinted = mAnswer6Tv.getText().toString();
                                    checkAnswer(answerHinted);
                                }
                            }
                        }
                    });
                }
            }
        }, 0, 1000);
    }

    private int getCurrentColumn() {
        if (mCurrentColumn % 3 == 0) {
            mCurrentColumn = 1;
        } else {
            ++mCurrentColumn;
        }
        return mCurrentColumn;
    }

    private void checkAnswer(final String answerHinted) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAnswerTv.setText(answerHinted);
                switch (answerType) {
                    case 1:
                        mAnswerV2Tv.setText("");
                        mAnswerV3Tv.setText("");
                        if (lstVerbModel.get(currentQuestionNo - 1).getEnglishVerb1().equalsIgnoreCase(answerHinted)) {
                            Toast.makeText(RoadAnimationActivity.this, "correct Answer", Toast.LENGTH_SHORT).show();
                            answerChb.setImageResource(R.drawable.tick);
                            mAnswerV1Tv.setText(answerHinted + " - ");
                            //                            mHeaderV1Tv.setBackgroundResource(R.color.yellow);
                            //                            mHeaderV2Tv.setBackgroundResource(R.color.red);
                            continueWrongHit = 0;
                            if (lifeRemaining < 10)
                                lifeRemaining++;
                            answerType++;
                        } else {
                            lifeRemaining--;
                            continueWrongHit++;
                            answerChb.setImageResource(R.drawable.wrong);
                        }
                        break;
                    case 2:
                        if (lstVerbModel.get(currentQuestionNo - 1).getEnglishVerb2().equalsIgnoreCase(answerHinted)) {
                            Toast.makeText(RoadAnimationActivity.this, "correct Answer", Toast.LENGTH_SHORT).show();
                            answerChb.setImageResource(R.drawable.tick);
                            mAnswerV2Tv.setText(answerHinted + " - ");
                            //                            mHeaderV2Tv.setBackgroundResource(R.color.yellow);
                            //                            mHeaderV3Tv.setBackgroundResource(R.color.red);
                            continueWrongHit = 0;
                            if (lifeRemaining < 10)
                                lifeRemaining++;
                            answerType++;
                        } else {
                            lifeRemaining--;
                            continueWrongHit++;
                            answerChb.setImageResource(R.drawable.wrong);
                        }
                        break;
                    case 3:
                        if (lstVerbModel.get(currentQuestionNo - 1).getEnglishVerb3().equalsIgnoreCase(answerHinted)) {
                            Toast.makeText(RoadAnimationActivity.this, "correct Answer", Toast.LENGTH_SHORT).show();
                            answerChb.setImageResource(R.drawable.tick);
                            mAnswerV3Tv.setText(answerHinted);
                            //                            mHeaderV3Tv.setBackgroundResource(R.color.yellow);
                            //                            mHeaderV1Tv.setBackgroundResource(R.color.red);
                            continueWrongHit = 0;
                            if (lifeRemaining < 10)
                                lifeRemaining++;
                            answerType = 1;
                            currentQuestionNo++;
                            setQuestion();
                        } else {
                            lifeRemaining--;
                            continueWrongHit++;
                            answerChb.setImageResource(R.drawable.wrong);
                        }
                        break;
                }
                mLifeRemainingTv.setText(String.valueOf(lifeRemaining));
                setLifeIndicator();
                if (lifeRemaining == 0) {
                    isPaused = true;
                    showAlertForOver();
                }
                if (continueWrongHit==5) {
                    isPaused = true;
                    continueWrongHit = 0;
                    showAlertFor5();
                }
            }
        });
    }

    private void setLifeIndicator() {
        switch (lifeRemaining) {
            case 1:
            case 2:
                lifeIndicatorIv.setImageResource(R.drawable.life1);
                break;
            case 3:
            case 4:
                lifeIndicatorIv.setImageResource(R.drawable.life2);
                break;
            case 5:
            case 6:
                lifeIndicatorIv.setImageResource(R.drawable.life3);
                break;
            case 7:
            case 8:
                lifeIndicatorIv.setImageResource(R.drawable.life4);
                break;
            case 9:
            case 10:
                lifeIndicatorIv.setImageResource(R.drawable.life5);
                break;
        }
    }

    private void showAlertFor5() {
        mDialog = DialogUtil.showOkCancelDialog(this, "Warning!", "Stop, you have attempt maximum hit! Do you want to continue or finish Lap?", finishListener, continueListener);
    }

    private void showAlertForOver() {
        mDialog = DialogUtil.showLapOverDialog(this, "Your life has finished. Better luck next time!", finishListener);
    }

    public void showSpeedometerDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.setContentView(R.layout.speedometer_dialog);
        ImageView slowerIv = (ImageView) dialog.findViewById(R.id.slower_iv);
        ImageView mediumIv = (ImageView) dialog.findViewById(R.id.medium_iv);
        ImageView fasterIv = (ImageView) dialog.findViewById(R.id.faster_iv);
        slowerIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeAllowedInSeconds=10;
                setAnimationTimings(10000);
                currSpeedInSecond=2200;
                inboundTimeLimit=6;
                outBoundTimeLimit=8;
                speedometerTimer.cancel();
                launchAnswerRegularly();
                dialog.dismiss();
            }
        });
        mediumIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeAllowedInSeconds=7;
                setAnimationTimings(7000);
                currSpeedInSecond=1500;
                inboundTimeLimit=4;
                outBoundTimeLimit=6;
                speedometerTimer.cancel();
                launchAnswerRegularly();
                dialog.dismiss();
            }
        });
        fasterIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeAllowedInSeconds=4;
                setAnimationTimings(4000);
                currSpeedInSecond=1100;
                inboundTimeLimit=2;
                outBoundTimeLimit=4;
                speedometerTimer.cancel();
                launchAnswerRegularly();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void setAnimationTimings(int milliseconds) {
        mAnswer1Ll.clearAnimation();
        mAnswer2Ll.clearAnimation();
        mAnswer3Ll.clearAnimation();
        mAnswer4Ll.clearAnimation();
        mAnswer5Ll.clearAnimation();
        mAnswer6Ll.clearAnimation();
        answers1Animation.setDuration(milliseconds);
        answers2Animation.setDuration(milliseconds);
        answers3Animation.setDuration(milliseconds);
        answers4Animation.setDuration(milliseconds);
        answers5Animation.setDuration(milliseconds);
        answers6Animation.setDuration(milliseconds);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.left_road_vw:
                mLeftCarVw.setVisibility(View.VISIBLE);
                mCenterCarVw.setVisibility(View.GONE);
                mRightCarVw.setVisibility(View.GONE);
                break;
            case R.id.center_road_vw:
                mLeftCarVw.setVisibility(View.GONE);
                mCenterCarVw.setVisibility(View.VISIBLE);
                mRightCarVw.setVisibility(View.GONE);
                break;
            case R.id.right_road_vw:
                mLeftCarVw.setVisibility(View.GONE);
                mCenterCarVw.setVisibility(View.GONE);
                mRightCarVw.setVisibility(View.VISIBLE);
                break;
            case R.id.pause_iv:
                if (!isPaused) {
                    mPauseIv.setImageResource(R.drawable.ic_play);
                } else {
                    mPauseIv.setImageResource(R.drawable.ic_pause);
                }
                isPaused = !isPaused;
                break;
            case R.id.accelerator_iv:
                showSpeedometerDialog();
                break;
        }
    }

    @Override
    public void onSuccess(int reqCode) {
        Log.d("lstVerbModel", lstVerbModel.toString());
        initialization();
        setQuestion();
        launchAnswerRegularly();
        validateAnswer();
    }

    @Override
    public void onCancel(boolean canceled) {

    }

    @Override
    protected void onPause() {
        super.onPause();
        isPaused = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isPaused = false;
    }
}