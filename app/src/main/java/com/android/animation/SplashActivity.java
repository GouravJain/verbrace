package com.android.animation;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;


public class SplashActivity extends BaseActivity {
    public static final String TAG = SplashActivity.class.getSimpleName();
    public static final int SPLASH_DELAY = 2500;

    private ImageView mSplashIv;
    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mSplashIv = (ImageView) findViewById(R.id.splash_iv);

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //startActivityWithAnim(LandingActivity.getIntent(SplashActivity.this));
                //finish();
            }
        }, SPLASH_DELAY);
        mSplashIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityWithAnim(LapMenuActivity.getIntent(SplashActivity.this));
            }
        });
    }


}
